#include <stdio.h>
#include <stdlib.h>


void funkce8(){
	char pole[8] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	char pom;
	for(int i = 0; i < 2; i++){
		if(i == 0){
			printf("  if %c < %c then\n", pole[0], pole[1]);
		}
		else{
			printf("  else\n");
		}
		//----------------------------
		for(int i3 = 2; i3 >= 0; i3--){
			if(i3 == 2){
				printf("    if %c < %c then\n", pole[1], pole[2]);
			}
			else if(i3 == 0){
				printf("    else\n");
			}
			else{
				printf("    else if %c < %c then\n", pole[(1+i3)%2], pole[(2+i3)%2]);
			}
			//----------------------
			for(int i4 = 3; i4 >= 0; i4--){
				if(i4 == 3){
					printf("      if %c < %c then\n", pole[2], pole[3]);
				}
				else if(i4 == 0){
					printf("      else\n");
				}
				else{
					printf("      else if %c < %c then\n", pole[(2+i4)%3], pole[(3+i4)%3]);
				}
				//-----------------------------------------
				for(int i5 = 4; i5 >= 0; i5--){
					if(i5 == 4){
						printf("        if %c < %c then\n", pole[3], pole[4]);
					}
					else if(i5 == 0){
						printf("        else\n");
					}
					else{
						printf("        else if %c < %c then\n", pole[(3+i5)%4], pole[(4+i5)%4]);
					}
					//-----------------------------------------
					for(int i6 = 5; i6 >= 0; i6--){
						if(i6 == 5){
							printf("          if %c < %c then\n", pole[4], pole[5]);
						}
						else if(i6 == 0){
							printf("          else\n");
						}
						else{
							printf("          else if %c < %c then\n", pole[(4+i6)%5], pole[(5+i6)%5]);
						}
						//-----------------------------------------
						for(int i7 = 6; i7 >= 0; i7--){
							if(i7 == 6){
								printf("            if %c < %c then\n", pole[5], pole[6]);
							}
							else if(i7 == 0){
								printf("            else\n");
							}
							else{
								printf("            else if %c < %c then\n", pole[(5+i7)%6], pole[(6+i7)%6]);
							}
							//-----------------------------------------
							for(int i8 = 7; i8 >= 0; i8--){
								if(i8 == 7){
									printf("              if %c < %c then\n", pole[6], pole[7]);
								}
								else if(i8 == 0){
									printf("              else\n");
								}
								else{
									printf("              else if %c < %c then\n", pole[(6+i8)%7], pole[(7+i8)%7]);
								}
								printf("                writeln(");
								for(int j = 0; j < 8; j++){
									if(j == 7){
										printf("%c", pole[j]);
									}
									else{
										printf("%c,", pole[j]);
									}
								}
								printf(")\n");
								if(i8 == 0){
									//preskladani zpet
									pom = pole[0];
									pole[0] = pole[1];
									pole[1] = pole[2];
									pole[2] = pole[3];
									pole[3] = pole[4];
									pole[4] = pole[5];
									pole[5] = pole[6];
								    pole[6] = pole[7];
									pole[7] = pom;
								}
								else{
									//zmena v poli
									pom = pole[i8];
									pole[i8] = pole[i8-1];
									pole[i8-1] = pom;
								}
							} // konec vnitrni urovne
							if(i7 == 0){
								//preskladani zpet
								pom = pole[0];
								pole[0] = pole[1];
								pole[1] = pole[2];
								pole[2] = pole[3];
								pole[3] = pole[4];
								pole[4] = pole[5];
								pole[5] = pole[6];
								pole[6] = pom;
							}
							else{
								//zmena v poli
								pom = pole[i7];
								pole[i7] = pole[i7-1];
								pole[i7-1] = pom;
							}
						}
						if(i6 == 0){
							//preskladani zpet
							pom = pole[0];
							pole[0] = pole[1];
							pole[1] = pole[2];
							pole[2] = pole[3];
							pole[3] = pole[4];
							pole[4] = pole[5];
							pole[5] = pom;
						}
						else{
							//zmena v poli
							pom = pole[i6];
							pole[i6] = pole[i6-1];
							pole[i6-1] = pom;
						}
					} 
					
					if(i5 == 0){
						//preskladani zpet
						pom = pole[0];
						pole[0] = pole[1];
						pole[1] = pole[2];
						pole[2] = pole[3];
						pole[3] = pole[4];
						pole[4] = pom;
					}
					else{
						//zmena v poli
						pom = pole[i5];
						pole[i5] = pole[i5-1];
						pole[i5-1] = pom;
					}

				}
				if(i4 == 0){
					//preskladani zpet
					pom = pole[0];
					pole[0] = pole[1];
					pole[1] = pole[2];
					pole[2] = pole[3];
					pole[3] = pom;
				}
				else{
					//zmena v poli
					pom = pole[i4];
					pole[i4] = pole[i4-1];
					pole[i4-1] = pom;
				}
			}
			//-----------------------
			//zmena v poli
			if(i3 > 0){
				pom = pole[i3];
				pole[i3] = pole[i3-1];
				pole[i3-1] = pom;
			}
			else{
				//preskladani...
				pole[0] = 'b';
				pole[1] = 'a';
				pole[2] = 'c';
				pole[3] = 'd';
				pole[4] = 'e';
				pole[5] = 'f';
				pole[6] = 'g';
				pole[7] = 'h';
			}
		}
	}
}





void funkce7(){
	char pole[7] = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};
	char pom;
	for(int i = 0; i < 2; i++){
		if(i == 0){
			printf("  if %c < %c then\n", pole[0], pole[1]);
		}
		else{
			printf("  else\n");
		}
		//----------------------------
		for(int i3 = 2; i3 >= 0; i3--){
			if(i3 == 2){
				printf("    if %c < %c then\n", pole[1], pole[2]);
			}
			else if(i3 == 0){
				printf("    else\n");
			}
			else{
				printf("    else if %c < %c then\n", pole[(1+i3)%2], pole[(2+i3)%2]);
			}
			//----------------------
			for(int i4 = 3; i4 >= 0; i4--){
				if(i4 == 3){
					printf("      if %c < %c then\n", pole[2], pole[3]);
				}
				else if(i4 == 0){
					printf("      else\n");
				}
				else{
					printf("      else if %c < %c then\n", pole[(2+i4)%3], pole[(3+i4)%3]);
				}
				//-----------------------------------------
				for(int i5 = 4; i5 >= 0; i5--){
					if(i5 == 4){
						printf("        if %c < %c then\n", pole[3], pole[4]);
					}
					else if(i5 == 0){
						printf("        else\n");
					}
					else{
						printf("        else if %c < %c then\n", pole[(3+i5)%4], pole[(4+i5)%4]);
					}
					//-----------------------------------------
					for(int i6 = 5; i6 >= 0; i6--){
						if(i6 == 5){
							printf("          if %c < %c then\n", pole[4], pole[5]);
						}
						else if(i6 == 0){
							printf("          else\n");
						}
						else{
							printf("          else if %c < %c then\n", pole[(4+i6)%5], pole[(5+i6)%5]);
						}
						//-----------------------------------------
						for(int i7 = 6; i7 >= 0; i7--){
							if(i7 == 6){
								printf("            if %c < %c then\n", pole[5], pole[6]);
							}
							else if(i7 == 0){
								printf("            else\n");
							}
							else{
								printf("            else if %c < %c then\n", pole[(5+i7)%6], pole[(6+i7)%6]);
							}
							printf("              writeln(");
							for(int j = 0; j < 7; j++){
								if(j == 6){
									printf("%c", pole[j]);
								}
								else{
									printf("%c,", pole[j]);
								}
							}
							printf(")\n");
							if(i7 == 0){
								//preskladani zpet
								pom = pole[0];
								pole[0] = pole[1];
								pole[1] = pole[2];
								pole[2] = pole[3];
								pole[3] = pole[4];
								pole[4] = pole[5];
								pole[5] = pole[6];
								pole[6] = pom;
							}
							else{
								//zmena v poli
								pom = pole[i7];
								pole[i7] = pole[i7-1];
								pole[i7-1] = pom;
							}
						} // konec vnitrni urovne
						if(i6 == 0){
							//preskladani zpet
							pom = pole[0];
							pole[0] = pole[1];
							pole[1] = pole[2];
							pole[2] = pole[3];
							pole[3] = pole[4];
							pole[4] = pole[5];
							pole[5] = pom;
						}
						else{
							//zmena v poli
							pom = pole[i6];
							pole[i6] = pole[i6-1];
							pole[i6-1] = pom;
						}
					} 
					
					if(i5 == 0){
						//preskladani zpet
						pom = pole[0];
						pole[0] = pole[1];
						pole[1] = pole[2];
						pole[2] = pole[3];
						pole[3] = pole[4];
						pole[4] = pom;
					}
					else{
						//zmena v poli
						pom = pole[i5];
						pole[i5] = pole[i5-1];
						pole[i5-1] = pom;
					}

				}
				if(i4 == 0){
					//preskladani zpet
					pom = pole[0];
					pole[0] = pole[1];
					pole[1] = pole[2];
					pole[2] = pole[3];
					pole[3] = pom;
				}
				else{
					//zmena v poli
					pom = pole[i4];
					pole[i4] = pole[i4-1];
					pole[i4-1] = pom;
				}
			}
			//-----------------------
			//zmena v poli
			if(i3 > 0){
				pom = pole[i3];
				pole[i3] = pole[i3-1];
				pole[i3-1] = pom;
			}
			else{
				//preskladani...
				pole[0] = 'b';
				pole[1] = 'a';
				pole[2] = 'c';
				pole[3] = 'd';
				pole[4] = 'e';
				pole[5] = 'f';
				pole[6] = 'g';
			}
		}
	}
}


void funkce6(){
	char pole[6] = {'a', 'b', 'c', 'd', 'e', 'f'};
	char pom;
	for(int i = 0; i < 2; i++){
		if(i == 0){
			printf("  if %c < %c then\n", pole[0], pole[1]);
		}
		else{
			printf("  else\n");
		}
		//----------------------------
		for(int i3 = 2; i3 >= 0; i3--){
			if(i3 == 2){
				printf("    if %c < %c then\n", pole[1], pole[2]);
			}
			else if(i3 == 0){
				printf("    else\n");
			}
			else{
				printf("    else if %c < %c then\n", pole[(1+i3)%2], pole[(2+i3)%2]);
			}
			//----------------------
			for(int i4 = 3; i4 >= 0; i4--){
				if(i4 == 3){
					printf("      if %c < %c then\n", pole[2], pole[3]);
				}
				else if(i4 == 0){
					printf("      else\n");
				}
				else{
					printf("      else if %c < %c then\n", pole[(2+i4)%3], pole[(3+i4)%3]);
				}
				//-----------------------------------------
				for(int i5 = 4; i5 >= 0; i5--){
					if(i5 == 4){
						printf("        if %c < %c then\n", pole[3], pole[4]);
					}
					else if(i5 == 0){
						printf("        else\n");
					}
					else{
						printf("        else if %c < %c then\n", pole[(3+i5)%4], pole[(4+i5)%4]);
					}
					//-----------------------------------------
					for(int i6 = 5; i6 >= 0; i6--){
						if(i6 == 5){
							printf("          if %c < %c then\n", pole[4], pole[5]);
						}
						else if(i6 == 0){
							printf("          else\n");
						}
						else{
							printf("          else if %c < %c then\n", pole[(4+i6)%5], pole[(5+i6)%5]);
						}
						printf("            writeln(");
						for(int j = 0; j < 6; j++){
							if(j == 5){
								printf("%c", pole[j]);
							}
							else{
								printf("%c,", pole[j]);
							}
						}
						printf(")\n");
						if(i6 == 0){
							//preskladani zpet
							pom = pole[0];
							pole[0] = pole[1];
							pole[1] = pole[2];
							pole[2] = pole[3];
							pole[3] = pole[4];
							pole[4] = pole[5];
							pole[5] = pom;
						}
						else{
							//zmena v poli
							pom = pole[i6];
							pole[i6] = pole[i6-1];
							pole[i6-1] = pom;
						}
					} // konec vnitrni urovne
					
					if(i5 == 0){
						//preskladani zpet
						pom = pole[0];
						pole[0] = pole[1];
						pole[1] = pole[2];
						pole[2] = pole[3];
						pole[3] = pole[4];
						pole[4] = pom;
					}
					else{
						//zmena v poli
						pom = pole[i5];
						pole[i5] = pole[i5-1];
						pole[i5-1] = pom;
					}

				}
				if(i4 == 0){
					//preskladani zpet
					pom = pole[0];
					pole[0] = pole[1];
					pole[1] = pole[2];
					pole[2] = pole[3];
					pole[3] = pom;
				}
				else{
					//zmena v poli
					pom = pole[i4];
					pole[i4] = pole[i4-1];
					pole[i4-1] = pom;
				}
			}
			//-----------------------
			//zmena v poli
			if(i3 > 0){
				pom = pole[i3];
				pole[i3] = pole[i3-1];
				pole[i3-1] = pom;
			}
			else{
				//preskladani...
				pole[0] = 'b';
				pole[1] = 'a';
				pole[2] = 'c';
				pole[3] = 'd';
				pole[4] = 'e';
				pole[5] = 'f';
			}
		}
	}
}


void funkce5(){
	char pole[5] = {'a', 'b', 'c', 'd', 'e'};
	char pom;
	for(int i = 0; i < 2; i++){
		if(i == 0){
			printf("  if %c < %c then\n", pole[0], pole[1]);
		}
		else{
			printf("  else\n");
		}
		//----------------------------
		for(int i3 = 2; i3 >= 0; i3--){
			if(i3 == 2){
				printf("    if %c < %c then\n", pole[1], pole[2]);
			}
			else if(i3 == 0){
				printf("    else\n");
			}
			else{
				printf("    else if %c < %c then\n", pole[(1+i3)%2], pole[(2+i3)%2]);
			}
			//----------------------
			for(int i4 = 3; i4 >= 0; i4--){
				if(i4 == 3){
					printf("      if %c < %c then\n", pole[2], pole[3]);
				}
				else if(i4 == 0){
					printf("      else\n");
				}
				else{
					printf("      else if %c < %c then\n", pole[(2+i4)%3], pole[(3+i4)%3]);
				}
				//-----------------------------------------
				for(int i5 = 4; i5 >= 0; i5--){
					if(i5 == 4){
						printf("        if %c < %c then\n", pole[3], pole[4]);
					}  
					else if(i5 == 0){
						printf("        else\n");
					}
					else{
						printf("        else if %c < %c then\n", pole[(3+i5)%4], pole[(4+i5)%4]);
					}

					printf("          writeln(");
					for(int j = 0; j < 5; j++){
						if(j == 4){
							printf("%c", pole[j]);
						}
						else{
							printf("%c,", pole[j]);
						}
					}
					printf(")\n");
					if(i5 == 0){
						//preskladani zpet
						pom = pole[0];
						pole[0] = pole[1];
						pole[1] = pole[2];
						pole[2] = pole[3];
						pole[3] = pole[4];
						pole[4] = pom;
					}
					else{
						//zmena v poli
						pom = pole[i5];
						pole[i5] = pole[i5-1];
						pole[i5-1] = pom;
					}

				}
				if(i4 == 0){
					//preskladani zpet
					pom = pole[0];
					pole[0] = pole[1];
					pole[1] = pole[2];
					pole[2] = pole[3];
					pole[3] = pom;
					
				}
				else{
					//zmena v poli
					pom = pole[i4];
					pole[i4] = pole[i4-1];
					pole[i4-1] = pom;
				}
			}
			//-----------------------
			//zmena v poli
			if(i3 > 0){
				pom = pole[i3];
				pole[i3] = pole[i3-1];
				pole[i3-1] = pom;
			}
			else{
				//preskladani...
				pole[0] = 'b';
				pole[1] = 'a';
				pole[2] = 'c';
				pole[3] = 'd';
				pole[4] = 'e';
			}
		}
	}
}

void funkce4(){
	char pole4[4] = {'a', 'b', 'c', 'd'};
	char pom;
	for(int i = 0; i < 2; i++){
		if(i == 0){
			printf("  if %c < %c then\n", pole4[0], pole4[1]);
		}
		else{
			printf("  else\n");
		}
		for(int i3 = 2; i3 >= 0; i3--){
			if(i3 == 2){
				printf("    if %c < %c then\n", pole4[1], pole4[2]);
			}
			else if(i3 == 0){
				printf("    else\n");
			}
			else{
				printf("    else if %c < %c then\n", pole4[(1+i3)%2], pole4[(2+i3)%2]);
			}
			//----------------------
			for(int i4 = 3; i4 >= 0; i4--){
				if(i4 == 3){
					printf("      if %c < %c then\n", pole4[2], pole4[3]);
				}
				else if(i4 == 0){
					printf("      else\n");
				}
				else{
					printf("      else if %c < %c then\n", pole4[(2+i4)%3], pole4[(3+i4)%3]);
				}
				printf("        writeln(");
				for(int j = 0; j < 4; j++){
					if(j == 3){
						printf("%c", pole4[j]);
					}
					else{
						printf("%c,", pole4[j]);
					}
				}
				printf(")\n");
				if(i4 == 0){
					//preskladani zpet
					pom = pole4[0];
					pole4[0] = pole4[1];
					pole4[1] = pole4[2];
					pole4[2] = pole4[3];
					pole4[3] = pom;
				}
				else{
					//zmena v poli
					pom = pole4[i4];
					pole4[i4] = pole4[i4-1];
					pole4[i4-1] = pom;
				}
			}
			//-----------------------
			//zmena v poli
			if(i3 > 0){
				pom = pole4[i3];
				pole4[i3] = pole4[i3-1];
				pole4[i3-1] = pom;
			}
			else{
				//preskladani...
				pole4[0] = 'b';
				pole4[1] = 'a';
				pole4[2] = 'c';
				pole4[3] = 'd';
			}
		}
	}
}

int main(){
	int pocet_vstupu = 0;
	scanf("%i", &pocet_vstupu);
	int pocet_cisel = 0;
	for(int i = 0; i < pocet_vstupu; i++){
		scanf("%i", &pocet_cisel);
		
		//zacatek kazdyho alg.
		printf("program sort(input,output);\n");
		printf("var\n");

		if(pocet_cisel == 1){
			//1
			printf("a : integer;\n");
			printf("begin\n");
			printf("  readln(a);\n");

			printf("  writeln(a)\n");
		}
		else if(pocet_cisel == 2){
			//2
			printf("a,b : integer;\n");
			printf("begin\n");
			printf("  readln(a,b);\n");

			printf("  if a < b then\n");
			printf("    writeln(a,b)\n");
			printf("  else\n");
			printf("    writeln(b,a)\n");
		}
		else if(pocet_cisel == 3){
			//3 - 3*2 = 6vypisu
			printf("a,b,c : integer;\n");
			printf("begin\n");
			printf("  readln(a,b,c);\n");
			
			printf("  if a < b then\n");//a,b
			printf("    if b < c then\n");
			printf("      writeln(a,b,c)\n");
			printf("    else if a < c then\n");
			printf("      writeln(a,c,b)\n");
			printf("    else\n");
			printf("      writeln(c,a,b)\n");
			printf("  else\n");//b,a
			printf("    if a < c then\n");
			printf("      writeln(b,a,c)\n");
			printf("    else if b < c then\n");
			printf("      writeln(b,c,a)\n");
			printf("    else\n");
			printf("      writeln(c,b,a)\n");
		}
		else if(pocet_cisel == 4){
			//4 - 6*4 = 24vypisu
			//vidim v tom smysl...
			printf("a,b,c,d : integer;\n");
			printf("begin\n");
			printf("  readln(a,b,c,d);\n");

			funkce4();
		}
		else if(pocet_cisel == 5){
			printf("a,b,c,d,e : integer;\n");
			printf("begin\n");
			printf("  readln(a,b,c,d,e);\n");
			funkce5();
		}
		else if(pocet_cisel == 6){
			printf("a,b,c,d,e,f : integer;\n");
			printf("begin\n");
			printf("  readln(a,b,c,d,e,f);\n");
			funkce6();
		}
		else if(pocet_cisel == 7){
			printf("a,b,c,d,e,f,g : integer;\n");
			printf("begin\n");
			printf("  readln(a,b,c,d,e,f,g);\n");
			funkce7();
		}
		//8
		else{
			printf("a,b,c,d,e,f,g,h : integer;\n");
			printf("begin\n");
			printf("  readln(a,b,c,d,e,f,g,h);\n");
			funkce8();
		}

		//konec kazdyho radiciho algoritmu
		if(i == pocet_vstupu-1){
			printf("end.\n");
		}
		//volny radek jen mezi jednotlivymi radicimi alg
		else{
			printf("end.\n\n");
		}
	}
	return 0;
}