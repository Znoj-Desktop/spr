#include <stdio.h>
#include <stdlib.h>
#include <math.h>

bool is_factorial_divide(int factorial, int number)
{
	//nelze delit nulou
	if (number == 0){
		return false;
	}
	//0! == 1, lze delit pouze 1
	if ((factorial == 0) && (number == 1)){
		return true;
	}
	
	if(factorial >= number){
		return true;
	}

	int max_number = sqrt((double)number) + 1;
	//iterace pres rozsah (prvo)cisel
	for (int i = 2; i < max_number; ++i) {
		//prvocislo vetsi nez faktorial jiste nedeli tento faktorial
		if(number > factorial){
			int counter = 0;
			//dokud lze delit beze zbytku, tak delim (delim pouze prvocisly, protoze ostatni neprojdou pres tuto podminku
			//nemohou delit to co uz nemohlo delit nizsi prvocislo
			while(number % i == 0){
				number /= i;
				counter++;
			}
			//printf(" -> prvocislo: %i, pocet: %i, zustatek: %i\n", i, counter, number);

			//projdu vsechny nasobky ve faktorialu a zjistim, jestli obsahuji stejna prvocisla
			//jaka jsem dostal z hledaneho cisla
			for(int fac_dec = factorial; fac_dec > 0; fac_dec--){
				int j = fac_dec;
				//nasel jsem vsechny, jdu na dalsi cislo
				if(counter == 0){
					break;
				}
				//neni sance aby tohle prvocislo bylo v danem faktorialu -> vracim false
				else if(j < i){
					//printf("j: %i < i: %i\n", j, i);
					return false;
				}
				//pokud muzu delit beze zbytku, delim
				while((j % i == 0) && (j >= i) && (counter > 0)){
					//printf("delim -> prvocislo: %i, pocet: %i, factorial: %i\n", i, counter, j);
					j /= i;
					counter--;
				}
			}

		}
	} //konec iterace pres vsechna (prvo)cisla
	return number <= factorial;
} 


int main(){
	
	int factorial = 0;
	int number = 0;

	while(scanf("%i %i", &factorial, &number) >= 0){
		if(is_factorial_divide(factorial , number)){
			printf("%i divides %i!\n", number, factorial);
		}
		else{
			printf("%i does not divide %i!\n", number, factorial);
		}
	}
	return 0;
}