#include <stdio.h>
#include <stdlib.h>

struct Slon{
	int vek;
	int iq;
	int poradi;
	int predchozi;
};

//globalni promenne
const int MAXSLONU = 1024;
Slon pole_slonu[MAXSLONU];
int memory[MAXSLONU];
int pocet_slonu = 0;
int pocet_vyhovujicich = 0;
int vystup[MAXSLONU];


//rekurzivni funkce
int Counting(int poradi, int vek, int iq){
	//posledni slon vraci 1
	if(poradi == pocet_slonu-1){
		memory[poradi] = 1;
		return memory[poradi];
	}
	//pokud jsem jiz zjistoval kolik nasledujicich slonu vyhovuje podmince,
	//tak to proste vratim
	if(memory[poradi] != 0){
		return memory[poradi];
	}

	memory[poradi] = 0;
	int pom = 0;
	for(int i = poradi+1; i < pocet_slonu; i++){
		//tato podminka je pro pripad ze bude vek stejny
		if(vek < pole_slonu[i].vek){
			//zameruju se jen na slony s nizsim iq
			if(iq > pole_slonu[i].iq){
				//ulozim si pocet navstivenych slonu
				pom = Counting(i, pole_slonu[i].vek, pole_slonu[i].iq);
				if(memory[poradi] < pom){
					pole_slonu[poradi].predchozi = i;
					memory[poradi] = pom;
				}
			}
			//pokracuju od zpracovavanyho slona
			else{
				//ulozim si pocet navstivenych slonu
				pom = Counting(i, pole_slonu[i].vek, pole_slonu[i].iq);
				if(memory[i] < pom){
					pole_slonu[i].predchozi = i;
					memory[i] = pom;
				}
			}
		}
	}
	//k poctu nasledujicich slonu prictu i tohoto a vracim se zpet
	memory[poradi] += 1;
	return memory[poradi];
}


void bubble_sort(){
    for(int i = 0; i < pocet_slonu - 1; i++){
        for(int j = 0; j < pocet_slonu - i - 1; j++){
            if(pole_slonu[j+1].vek < pole_slonu[j].vek){
                Slon tmp = pole_slonu[j + 1];
                pole_slonu[j + 1] = pole_slonu[j];
                pole_slonu[j] = tmp;
            }   
        }   
    }   
}


int main(){
	int prvni_cislo;
	int druhy_cislo;

	while(scanf("%i", &prvni_cislo) > 0){
		scanf("%i", &druhy_cislo);

		pole_slonu[pocet_slonu].vek = prvni_cislo; 
		pole_slonu[pocet_slonu].iq = druhy_cislo;
		pole_slonu[pocet_slonu].poradi = pocet_slonu++;
		pole_slonu[pocet_slonu].predchozi = 0;
		//printf("%i %i\n", prvni_cislo, druhy_cislo);
	}

	//serazeni slonu podle veku
	bubble_sort();
	
	for(int i = 0; i < pocet_slonu; i++){
		memory[i] = 0;
	}

	//zavolani funkce a ulozeni prvniho poctu slonu
	pocet_vyhovujicich = Counting(0, pole_slonu[0].vek, pole_slonu[0].iq);
	
	int prvni_slon = 0;
	for(int i = 0; i < pocet_slonu; i++){
		if(memory[i] > pocet_vyhovujicich){ 
			pocet_vyhovujicich = memory[i];
			prvni_slon = i;
		}
		//printf("%i\n", memory[i]);
	}
	
	
	int ukazatel_vystup = 0;
	//vyhledam si vsechny slony vyhovujici podmince a zapisu si jejich puvodni poradi do pole
	for(int i = prvni_slon; i != 0;){
		vystup[ukazatel_vystup++] = pole_slonu[i].poradi;
		i = pole_slonu[i].predchozi;
	}

	printf("%i\n", pocet_vyhovujicich);
	for(int i = 0; i < pocet_vyhovujicich; i++){
		//+1 proto, protoze to mam cislovany od 0
		printf("%i\n", vystup[i]+1);
	}

	return 0;
}