#include <stdio.h>
#include <stdlib.h>

int main(){
	int puvodni;
	//nacitani po celych cislech
	while(scanf("%i", &puvodni) == 1){
			unsigned char *data = (unsigned char *) &puvodni;
			//3 2 1 0 -> 0 1 2 3, bitovy OR pro logicky soucet vsech "casti"
			int novy = (data[3]<<0) | (data[2]<<8) | (data[1]<<16) | (data[0]<<24);
			printf("%i converts to %i\n", puvodni, novy);
	}
	return 0;
}