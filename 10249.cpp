#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <queue>

using namespace std;

#define MAX_TEAMS 70
#define MAX_TABLES 50

typedef queue<int>  INTQUEUE;

struct TeamStruct{
	int poradi;
	int hodnota;
	INTQUEUE q;
};

struct TableStruct{
	int poradi;
	int hodnota;
};


TeamStruct TeamsMembers[MAX_TEAMS];
TableStruct TablesCapacity[MAX_TABLES];
int PoleSerazeniFronty[MAX_TEAMS];

//inicializace poli
void inicializace(int teams, int tables){
	for(int i = 0; i < MAX_TEAMS; i++){
		PoleSerazeniFronty[i] = 0;
		TeamsMembers[i].hodnota = 0;
		TeamsMembers[i].poradi = 0;
	}

	for(int i = 0; i < MAX_TABLES; i++){
		TablesCapacity[i].hodnota = 0;
		TablesCapacity[i].poradi = 0;
	}

	for(int i = 0; i < teams; i++){
		cin >> TeamsMembers[i].hodnota;
		TeamsMembers[i].poradi = i;
	}

	for(int i = 0; i < tables; i++){
		cin >> TablesCapacity[i].hodnota;
		TablesCapacity[i].poradi = i;
	}
}


//alg pro serazeni
void TeamsSort(int max){	
	for(int i = 0; i < max - 1; i++){
		for(int j = 0; j < max - i - 1; j++){
			if(TeamsMembers[j+1].hodnota > TeamsMembers[j].hodnota){
				TeamStruct tmp = TeamsMembers[j+1];
				TeamsMembers[j+1] = TeamsMembers[j];
				TeamsMembers[j] = tmp;
			}  
		}  
	}
}

void TableSort(int max){	
	for(int i = 0; i < max - 1; i++){
		for(int j = 0; j < max - i - 1; j++){
			if(TablesCapacity[j+1].hodnota > TablesCapacity[j].hodnota){
				TableStruct tmp = TablesCapacity[j+1];
				TablesCapacity[j+1] = TablesCapacity[j];
				TablesCapacity[j] = tmp;
			}  
		}  
	}
}

void TeamsSortPoradi(int max){	
	for(int i = 0; i < max - 1; i++){
		for(int j = 0; j < max - i - 1; j++){
			if(TeamsMembers[j+1].poradi < TeamsMembers[j].poradi){
				TeamStruct tmp = TeamsMembers[j+1];
				TeamsMembers[j+1] = TeamsMembers[j];
				TeamsMembers[j] = tmp;
			}  
		}  
	}
}

void TableSortPoradi(int max){	
	for(int i = 0; i < max - 1; i++){
		for(int j = 0; j < max - i - 1; j++){
			if(TablesCapacity[j+1].poradi < TablesCapacity[j].poradi){
				TableStruct tmp = TablesCapacity[j+1];
				TablesCapacity[j+1] = TablesCapacity[j];
				TablesCapacity[j] = tmp;
			}  
		}  
	}
}

void SerazeniFronty(int max){	
	for(int i = 0; i < max - 1; i++){
		for(int j = 0; j < max - i - 1; j++){
			if(PoleSerazeniFronty[j+1] < PoleSerazeniFronty[j]){
				int tmp = PoleSerazeniFronty[j+1];
				PoleSerazeniFronty[j+1] = PoleSerazeniFronty[j];
				PoleSerazeniFronty[j] = tmp;
			}  
		}  
	}

	for(int i = 0; i < max; i++){
		if(i != 0){
			cout << " " << PoleSerazeniFronty[i];
		}
		else{
			cout << PoleSerazeniFronty[i];
		}
	}
}

int main(){
	int teams = 0;
	int tables = 0;
	
	cin >> teams;
	cin >> tables;

	if(teams == 0 && tables == 0){
		return 0;
	}

	while(1){

		int nelze = 0;
		int PocetVeFronte = 0;

		//inicializace poli
		inicializace(teams, tables);

		//serazeni podle velikosti
		TeamsSort(teams);
		TableSort(tables);

		//od nejvyssiho cisla odcitam jednicku, od dalsiho v poradi taky, ... aby sedel kazdy clen z prave zpracovavaneho
		//(nejvetsiho jeste nezpracovaneho) tymu u jineho stolu. Ukladam pak do fronty tymu cislo stolu u ktereho
		//nekdo z daneho tymu sedi
		for(int ZpracovavanyTym = 0; ZpracovavanyTym < teams; ZpracovavanyTym++){
			for(int i = 0; i < TeamsMembers[ZpracovavanyTym].hodnota; i++){
				TablesCapacity[i].hodnota--;
				if(TablesCapacity[i].hodnota < 0){
					nelze = 1;
					break;
				}
				TeamsMembers[ZpracovavanyTym].q.push(TablesCapacity[i].poradi);
			}
			//znovu preskladam
			TableSort(tables);
			if(nelze){
				break;
			}
		}

		//serazeni podle puvodniho poradi
		TableSortPoradi(tables);
		TeamsSortPoradi(teams);

		//pokud je pocet lidi v nejvetsim tymu mensi jak pocet stolu a neni uveden priznak neresitelnosti prikladu, pak...
		if(!nelze && TeamsMembers[0].hodnota <= tables){
			printf("1\n");
			for(int i = 0; i < teams; i++){
				//prekopirovani z fronty do pole kvuli naslednemu serazeni
				while(!TeamsMembers[i].q.empty()){
					PoleSerazeniFronty[PocetVeFronte++] = TeamsMembers[i].q.front()+1;
					TeamsMembers[i].q.pop();
				}
				//serazeni cisel fronty podle abecedy
				// + vypsani cisel ve fronte v pozadovanem tvaru
				SerazeniFronty(PocetVeFronte);

				if(i != teams-1){
					printf("\n");
				}
				PocetVeFronte = 0;
			}
		}

		else{
			printf("0");
			for(int i = 0; i < teams; i++){
				while(!TeamsMembers[i].q.empty()){
					TeamsMembers[i].q.pop();
				}
			}
		}

		//nacteni cisel a pripadne konec
		cin >> teams;
		cin >> tables;
		if(teams == 0 && tables == 0){
			return 0;
		}
		else{
			printf("\n");
		}
	}
}