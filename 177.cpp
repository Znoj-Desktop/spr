#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

//velky pole, max je 13
//pro 13 je nejlevejsi hodnota o 170 nizsi nez pocatecni a o 84 vyse polozena -> pocatek y = 84, x = 170
//pro 13 je maximalni sirka 255 a maximalni vyska mensi
#define pocatek_y 84
#define pocatek_x 170
#define max 255

//globalni promenne
int pole[max][max];
int pole2[max][max];

//inicializace na pocatecni velikost pole
int od_y = pocatek_y-2;
int do_y = pocatek_y;
int od_x = pocatek_x;
int do_x = pocatek_x+2;
int konec_od_y = 0;
int konec_od_x = 0;

//souradnice posledniho prvku v obrazci
int rozdil_x = 0;
int rozdil_y = 0;


void inicializace(){
	//vynulovani celeho pole
	for(int i = 0; i < max; i++){
		for(int j = 0; j < max; j++){
			pole[i][j] = 0;
			pole2[i][j] = 0;
		}
	}
	od_y = pocatek_y-2;
	do_y = pocatek_y;
	od_x = pocatek_x;
	do_x = pocatek_x+2;
}

//vlozeni 1. prelozeni do pole
void pocatek_do_pole(){
	//vlozeni znaku pro 1
	//'_'
	pole[pocatek_y][pocatek_x] = 1;
	pole[pocatek_y][pocatek_x+1] = 1;
	pole[pocatek_y][pocatek_x+2] = 1;
	//'|'
	//pole[max/2][max/2+2] = 1;
	pole[pocatek_y-1][pocatek_x+2] = 1;
	pole[pocatek_y-2][pocatek_x+2] = 1;
}


void prekopirovani_pole(bool priznak){
	for(int i = od_y; i < do_y+1; i++){
		for(int j = od_x; j < do_x+1; j++){
			//z pole do pole2 + hledani koncoveho prvku
			if(priznak){
				pole2[i][j] = pole[i][j];
				if(pole[i][j] == 1 && pole[i-1][j] == 0 && pole[i][j+1] == 0 && pole[i][j-1] == 0){
					//zaznamenam si souradnice posledniho prvku kvuli posunuti otoceneho obrazce
					rozdil_y = i;
					rozdil_x = j;
				}
			}
			//z pole2 do pole
			else{
				//pole 2 pouzivam proto, abych si neprepisoval pole pod rukama
				pole[i][j] = pole2[i][j];
			}
		}
	}
}


void zakresleni(){
	//promenne pro nove souradnice pootocenych obrazcu
	int novy_x = 0;
	int novy_y = 0;
	for(int i = od_y; i < do_y+1; i++){
		for(int j = od_x; j < do_x+1; j++){
			if(pole[i][j] == 1){
				//vypocteni novych souradnic
				novy_y = j-rozdil_x;
				novy_x = max-i-rozdil_y;
				//vykresleni do novych souradnic
				pole2[novy_y][novy_x] = 1;

				//podminky pro zaznamenani rozsahu pole
				if(novy_x < od_x ){
					od_x = novy_x;
				}
				if(do_x < novy_x){
					do_x = novy_x;
				}
				if(novy_y < od_y){
					od_y = novy_y;
				}
				if(do_y < novy_y){
					do_y = novy_y;
				}
			}
		}
	}
}


void sloz_papir(){
	//priznak 1 rika, ze kopiruju z pole do pole2 a zaroven hledam koncovy prvek
	prekopirovani_pole(1);
	
	int pom = rozdil_x;
	//vypocteni souradnic pro spravne usazeni pootoceneho obrazce
	rozdil_x = rozdil_x - rozdil_y;
	rozdil_y = max - pom - rozdil_y;
	
	//zaznamenani pootoceneho obrazce do pole + urceni souradnic
	zakresleni();

	//priznak 0 znamena ze jen kopiruju z pole2 zpet do pole1 ale i s vysledkem
	//je nutne pouzivat 2 pole, protoze jinak bych mohl cast vysledneho obrazce znovu pretocit a zakreslit
	prekopirovani_pole(0);
}

void prevedeni_na_znaky(){
	int prvni_vypsat = 0;
	for(int i = od_y; i < do_y+1; i++){
		for(int j = od_x; j < do_x+1; j++){
			//vypisuji prvni a pak kazdy sudy radek
			if(i == od_y || i%2 == od_y%2+1){
				//pokud v prvnim radku neni "_" pak radek preskoc
				if(i == od_y){
					for(int o = od_x; o < do_x+1; o++){
						if(pole[i][o] == 0 && pole[i+1][o] && pole[i+1][o-1] && pole[i+1][o+1]){
							prvni_vypsat = 1;
							break;
						}
						else if(pole[i][o] && pole[i][o-1] && pole[i][o+1]){
							prvni_vypsat = 1;
							break;
						}
					}
					if(!prvni_vypsat){
						i++;
					}
				}

				if(pole[i][j] == 0 && pole[i+1][j] && pole[i+1][j-1] && pole[i+1][j+1]){
					printf("_");
				}
				else if(pole[i][j] && pole[i][j-1] && pole[i][j+1]){
					printf("_");
				}
				else if(i != od_y && pole[i][j] && pole[i-1][j] && pole[i+1][j]){
					printf("|");
				}
				else{
					//pokud do konce radku existuje jeste nejaky znak ktery je treba vypsat, pak vypis mezeru
					for(int k = j+1; k < do_x+1; k++){
						if(pole[i][k] || (pole[i+1][k] && pole[i+1][k-1] && pole[i+1][k+1])){
							printf(" ");
							break;
						}
					}
					//printf(".");
				}
			}
		}
		//jen pro vypisovane radky odradkuju
		if((i == od_y) || i%2 == od_y%2+1){
			printf("\n");
		}
	}

}

int main(){
	//nacteni cisla
	int cislo;
	cin >> cislo;

	while(cislo != 0){

		inicializace();

		pocatek_do_pole();

		//for cyklus od 1, protoze prvni cyklus je jiz hotov
		for(int i = 1; i < cislo; i++){
			sloz_papir();
		}
	
		//prevod z 1 a 0 na znaky '_' a '|'
		if(cislo != 1){
			prevedeni_na_znaky();
		}
		else{
			printf("_|\n");
		}
		//vypis pole
		/*
		for(int i = od_y; i < do_y+1; i++){
			for(int j = od_x; j < do_x+1; j++){
				printf("%i", pole[i][j]);
			}
			printf("\n");
		}
		*/

		//nacteni dalsiho cisla
		cin >> cislo;
		if(cislo == 0){
			printf("^\n");
			return 0;
		}
		else{
			//od radkovani za celym obrazcem
			printf("^\n");
		}
	}
}