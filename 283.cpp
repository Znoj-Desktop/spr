#include <stdio.h>
#include <stdlib.h>

#define ASCII 256
#define MAX_INT 2147483647
int cela_tabulka[ASCII];
int vstup[ASCII];
int pocet_znaku;
int reseni[ASCII];


int velikost_kodu(){
	int celkem_listu = 0;
	for(int i = 0; i < pocet_znaku; i++){
		celkem_listu += vstup[i];
		//nejmensi hodnotu inicializujeme na maximalni moznou
		int nejmensi_hodnota = MAX_INT;
		int vyska = 1;
		int mist_ve_stromu;
		//promenna x rika, jestli ma smysl vytvaret vyssi strom
		int x = 0;
		//cyklus pro ruzne vysky stromu
		do{
			int mocnina = 1;
			//2^vyska
			for(int h = 0; h < vyska; h++){
				mocnina *= 2;
			}
			mist_ve_stromu = mocnina - 1;
			x = i - mist_ve_stromu;
			//pokud v celkem_listu je vice listu nez se vleze do prave zpracovavaneho stromu,
			//tak se ke kazdemu listu pricte krome teto vysky jeste vyska podstromu
			//ve kterem list lezi
			int nova_hodnota = vyska * celkem_listu;
			//pokud je hodnota x kladna, tak mame vice znaku nez se vleze do prave
			//vytvareneho stromu -> je potreba pricist predchozi podstrom
			if(x > 0){
				nova_hodnota += reseni[x];
			}
			if(nova_hodnota < nejmensi_hodnota){
				nejmensi_hodnota = nova_hodnota;
			}
			vyska++;
		} while(x > 0);
		reseni[i] = nejmensi_hodnota;
	}
	return reseni[pocet_znaku-1];
}


void bubbleSort(int * pole, int velikost){
	for(int i = 0; i < velikost - 1; i++){
		for(int j = 0; j < velikost - i - 1; j++){
			if(pole[j+1] < pole[j]){
				int tmp = pole[j + 1];
				pole[j + 1] = pole[j];
				pole[j] = tmp;
			}  
		}  
	}  
} 

int main(){
	int pocet_vstupu;
	int pocet_radku;
	int znak = NULL;
	
	scanf("%d", &pocet_vstupu);
	while(pocet_vstupu > 0){
		scanf("%d", &pocet_radku);

		//vynulovani predchozich hodnot
		for(int i = 0; i < ASCII; i++){
			cela_tabulka[i] = 0;
		}
		pocet_znaku = 0;
		pocet_vstupu--;

		//nacteni poctu znaku do tabulky
		for(int j = 0; j < pocet_radku; j++){
			if(znak == '$'){
				znak = getchar();
			}
			while(znak != '$'){
				znak = getchar();
				cela_tabulka[znak] += 1;
			}
		}

		//rozsah povolenych znaku je 32...122
		//povulujeme vic znaku nez je v zadani, ale to neva
		for(int k = 32; k < 123; k++){
			if(cela_tabulka[k] > 0){
				vstup[pocet_znaku++] = cela_tabulka[k];
			}
		}
		
		//serazeni znaku podle velikosti
		bubbleSort(vstup, pocet_znaku);

		/*
		for(int l = 0; l < pocet_znaku; l++){
			printf("%d\n", vstup[l]);
		}
		*/

		printf("%d\n", velikost_kodu());
	}
}