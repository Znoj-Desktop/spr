#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

#define MAX_NUMBER 2147483647
#define MAX_DIGITS 256
#define MOZNOSTI 10

char vstup[MAX_DIGITS];
long long memory[MAX_DIGITS];
int DelkaRetezce;

//pocitani maximalniho cisla slozenyho ze zadanych cislic
long long Counting(int zacatek){
	//pokud cislice jeste nebyla zpracovana, zpracuj
    if(memory[zacatek] >= 0){
		//vrat maximum z predeslyho vypoctu, nebo 0, pokud jsem narazil na konec sekvence cislic
		return memory[zacatek];
	}
	//je potreba lokalni promennou umistit zde, abych si ju neprepisoval
    long long celkem = 0;

	//zpracuj postupne od teto cislice vsech 10 moznosti (1+1145678908 ... 1114567890+8)
	for(int konec = 0; konec < MOZNOSTI; konec++){
		//podminka at nesaham mimo pole - konci cyklus
		if((zacatek + konec) >= DelkaRetezce){
			break;
		}
		celkem = celkem * 10 + vstup[zacatek + konec] - '0';
		if(celkem >= MAX_NUMBER){
			break;
		}
		//zanorim se pro vysledek pro cislo vpravo
		long long pom = celkem + Counting(zacatek + konec + 1);

		//postupne ukladani vysledku do pameti, pokud je vetsi jak prave ziskany
		//tak zustava
		if(pom > memory[zacatek]){
			memory[zacatek] = pom;
		}
	}
	
	return memory[zacatek];
}

int main(void){
	int PocetVstupu = 0;
	scanf("%d", &PocetVstupu);

	for(int i = 0; i < PocetVstupu; i++) {
		//nacteni celyho cisla
		scanf("%s", vstup);
		DelkaRetezce = strlen(vstup);

		//hodim si priznaky do pameti -> -1 proto, protoze 0 pouzivam jako cislo
		//a taky jako "brzdu" pro zanorovani
		for(int k = 0; vstup[k]; ++k){
			memory[k] = -1;
		}
		//oznacim si posledni cislo jako onu brzdu pro zanorovani
		memory[DelkaRetezce] = 0;

		//zavolani rekurzivni funkce pro vypocet
		long long a = Counting(0);

		//vypsani vystupu
		printf("%lld\n", a);
	}
    return 0;
}