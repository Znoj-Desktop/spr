#include <stdlib.h>
#include <stdio.h>

//zde se uklada cislo udavajici co ma hrac v rukach
//1 = High Card (nejvyšši karta)
//2 = Pair (par)
//3 = Two Pairs (2 pary)
//4 = Three of a kind (trojice)
//5 = Straight (postupka)
//6 = Flush (barva)
//7 = Full House (dvojice + trojice)
//8 = Four of a kind (ctverice)
//9 = Straight flush (postupka v barve)
int ohodnoceni_karet(int pole[10], int &nejvyssi_karta, int &druha_nejvyssi, int &treti_nejvyssi){
	//jestlize postupka...
	if(pole[0]+1 == pole[2] && pole[2]+1 == pole[4] && pole[4]+1 == pole[6] && pole[6]+1 == pole[8]){
		if((pole[1] == pole[3]) && (pole[3] == pole[5]) && (pole[5] == pole[7]) && (pole[7] == pole[9])){
			//printf("postupka v barve\n");
			return 9;
		}
		//printf("postupka\n");
		return 5;
	}
	
	// zkoumani paru, pokud prvni dve karty jsou v paru
	else if(pole[0] == pole[2]){
		//pokud je treti karta s druhou v paru, tak v tomto pripade mame trojici
		if(pole[2] == pole[4]){
			//pokud je zde 3. a 4. karta se stejnou hodnotou, tak mame ctverici
			if(pole[4] == pole[6]){
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[0];
				//printf("ctverice\n");
				return 8;
			}
			//pokud je 4. a 5. karta v paru, pak zde mame full house
			else if(pole[6] == pole[8]){
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[0];
				//printf("full house\n");
				return 7;
			}
			else{
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[0];
				//printf("trojice\n");
				return 4;
			}
		}
		//pokud je treti karta se ctvrtou v paru
		else if(pole[4] == pole[6]){
			//pokud je i 4. a 5. karta v paru, pak mame fullhouse
			if(pole[6] == pole[8]){
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[4];
				//printf("full house\n");
				return 7;
			}
			else{
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[4];
				//-------------------------------------------------------------------zde je potreba uvezt 2. nejvyssi kartu
				druha_nejvyssi = pole[0];
				//-------------------------------------------------------------------zde je potreba uvezt 3. nejvyssi kartu
				treti_nejvyssi = pole[8];
				//printf("dva pary\n");
				return 3;
			}
			
		}
		//pokud je 4. a 5. karta v paru, tak v tomto pripade jde o 2 ddvojice
		else if(pole[6] == pole[8]){
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[6];
			//-------------------------------------------------------------------zde je potreba uvezt 2. nejvyssi kartu
			druha_nejvyssi = pole[0];
			//-------------------------------------------------------------------zde je potreba uvezt 3. nejvyssi kartu
			treti_nejvyssi = pole[4];
			//printf("dva pary\n");
			return 3;
		}
		
		else{
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[0];
			//printf("par\n");
			return 2;
		}
	}
	
	//pokud je druha karta s treti v paru, tak mame par
	else if(pole[2] == pole[4]){
		//pokud je zde 3. a 4. karta se stejnou hodnotou, tak mame trojici
		if(pole[4] == pole[6]){
			if(pole[6] == pole[8]){
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[2];
				//printf("ctverice\n");
				return 8;
			}
			else{
				//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
				nejvyssi_karta = pole[2];
				//printf("trojice\n");
				return 4;
			}
		}
		//pokud je 4. a 5. karta v paru, 2 dvojice
		else if(pole[6] == pole[8]){
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[6];
			//-------------------------------------------------------------------zde je potreba uvezt 2. nejvyssi kartu
			druha_nejvyssi = pole[2];
			//-------------------------------------------------------------------zde je potreba uvezt 3. nejvyssi kartu
			treti_nejvyssi = pole[0];
			//printf("dva pary\n");
			return 3;
		}
		else{
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[2];
			//printf("par\n");
			return 2;
		}
	}
	
	//zkoumani barvy
	else if((pole[1] == pole[3]) && (pole[3] == pole[5]) && (pole[5] == pole[7]) && (pole[7] == pole[9])){
			//printf("barva\n");
			return 6;
	}
	
	else if(pole[4] == pole[6]){
		//pokud je i 4. a 5. karta v paru, pak mame trojici
		if(pole[6] == pole[8]){
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[4];
			//printf("trojice\n");
			return 4;
		}
		else{
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[4];
			//printf("par\n");
			return 2;
		}
			
	}
	
	//zde muze byt uz pozze jedna dvojice = par
	else if(pole[6] == pole[8]){
			//-------------------------------------------------------------------zde je potreba uvezt nejvyssi kartu
			nejvyssi_karta = pole[6];
			//printf("par\n");
			return 2;
	}
	
	//v ruce neni zadna dvojice ani nic lepsiho
	return 1;
	
	
} // konec ohodnoceni karet

//prepise ascii znaky na hodnoty karet, pricemz T = 10, J = 11, Q = 12, K = 13 a A = 14
int prepis_znaku(int znak){
	if((znak > '1') && (znak <= '9')){
		return (znak - 48);
	}
	else if(znak == 'T'){
		return 10;
	}
	else if(znak == 'J'){
		return 11;
	}
	else if(znak == 'Q'){
		return 12;
	}
	else if(znak == 'K'){
		return 13;
	}
	else{
		return 14;
	}
}

int main(){
	int znak;
	int pole1[10];
	int pole2[10];
	znak = getchar();
	while(1){
		int pocitadlo = 0;
		while(pocitadlo < 10){
			//preskakovani bordelu
			while(!((znak >= '2' && znak <= '9')||(znak == 'T' || znak == 'J' || znak == 'Q' || znak == 'K' || znak == 'A'))) {
				if(znak == -1){
//konec					//zde je konec
					return 0;
				}
				znak = getchar();  
			}
			//prepis znaku na cisla
			znak = prepis_znaku(znak);
			pole1[pocitadlo++] = znak;
			znak = getchar();
			//preskakovani bordelu
			while(znak != 'C' && znak != 'D' && znak != 'H' && znak != 'S'){
				znak = getchar();
			}
			pole1[pocitadlo++] = znak;
		}
		
		//druhej hrac - ta sama pisnicka
		pocitadlo = 0;
		while(pocitadlo < 10){
			//preskakovani bordelu
			while(!((znak >= '2' && znak <= '9')||(znak == 'T' || znak == 'J' || znak == 'Q' || znak == 'K' || znak == 'A'))) {
				znak = getchar();  
			}
			//prepis znaku na cisla
			znak = prepis_znaku(znak);
			pole2[pocitadlo++] = znak;
			znak = getchar();
			//preskakovani bordelu
			while(znak != 'C' && znak != 'D' && znak != 'H' && znak != 'S'){
				znak = getchar();
			}
			pole2[pocitadlo++] = znak;
		}
		
		//serazeni poli - vlastni razeni (pro 2x5 polozek dostacujici)
		int nejmensi = pole1[0];
		int pom = 0;
		//serazeni karet prvniho hrace
		for(int j = 0; j < 10;j+=2){
			nejmensi = pole1[j];
			for(int i = j;i < 10;i+=2){
				if(pole1[i] < nejmensi){
					pom = pole1[i];
					pole1[i] = nejmensi;
					nejmensi = pom;
				}
			}
			pole1[j] = nejmensi;
		}
		
		//serazeni karet druhyho hrace
		for(int j = 0; j < 10;j+=2){
			nejmensi = pole2[j];
			for(int i = j;i < 10;i+=2){
				if(pole2[i] < nejmensi){
					pom = pole2[i];
					pole2[i] = nejmensi;
					nejmensi = pom;
				}
			}
			pole2[j] = nejmensi;
		}
		
		//promenny pro pripad, ze maji hraci v ruce "to stejny" = napr. oba postupku, nebo oba flash, ...
		int nejvyssi1 = pole1[8];
		int nejvyssi11 = 0;
		//pro pripad dvou dvojic
		int nejvyssi111 = 0;

		int nejvyssi2 = pole2[8];
		int nejvyssi22 = 0;
		int nejvyssi222 = 0;
		
		//zde se uklada cislo udavajici co ma hrac v rukach
		//1 = High Card (nejvyšši karta)
		//2 = Pair (par)
		//3 = Two Pairs (2 pary)
		//4 = Three of a kind (trojice)
		//5 = Straight (postupka)
		//6 = Flush (barva)
		//7 = Full House (dvojice + trojice)
		//8 = Four of a kind (ctverice)
		//9 = Straight flush (postupka v barve)
		
		int hodnoceni1 = ohodnoceni_karet(pole1, nejvyssi1, nejvyssi11, nejvyssi111);
		int hodnoceni2 = ohodnoceni_karet(pole2, nejvyssi2, nejvyssi22, nejvyssi222);

		if(hodnoceni1 > hodnoceni2){
			printf("Black wins.\n");
		}
		else if(hodnoceni1 < hodnoceni2){
			printf("White wins.\n");
		}
		//rovnaji se
		else{
			//porovnani hracu, kteri maji 2 pary
			if(hodnoceni1 == 3){
				//podle vyssiho paru
				if(nejvyssi1 > nejvyssi2){
					printf("Black wins.\n");
				}
				else if(nejvyssi1 < nejvyssi2){
					printf("White wins.\n");
				}
				else{
					//podle nizsiho paru
					if(nejvyssi11 > nejvyssi22){
						printf("Black wins.\n");
					}
					else if(nejvyssi11 < nejvyssi22){
						printf("White wins.\n");
					}
					else{
						//podle neparovyho cisla
						if(nejvyssi111 > nejvyssi222){
							printf("Black wins.\n");
						}
						else if(nejvyssi111 < nejvyssi222){
							printf("White wins.\n");
						}
						//vsechna cisla stejna
						else{
							printf("Tie.\n");
						}
					}
				}
			}
			//porovnani hracu s 1 parem
			else if(hodnoceni1 == 2){
				//podle paru
				if(nejvyssi1 > nejvyssi2){
					printf("Black wins.\n");
				}
				else if(nejvyssi1 < nejvyssi2){
					printf("White wins.\n");
				}
				//podle zbyvajicich karet
				else{
					int vyhravajici_hrac = 0;
					for(int i = 8; i >= 0; i -= 2){
						//pokud tato karta nepatri ani u jednoho z hracu do dvojice, pak ji muzeme porovnat
						if((pole1[i] != nejvyssi1) && (pole2[i] != nejvyssi1)){
							if(pole1[i] > pole2[i]){
								vyhravajici_hrac = 1;
							}
							else if(pole1[i] < pole2[i]){
								vyhravajici_hrac = 2;
							}
						}
						else if(pole1[i] != nejvyssi1){
							vyhravajici_hrac = 1;
							break;
						}
						else if(pole2[i] != nejvyssi1){
							vyhravajici_hrac = 2;
							break;
						}
						if(vyhravajici_hrac != 0){
							break;
						}
					}
					if(vyhravajici_hrac == 1){
						printf("Black wins.\n");
					}
					else if(vyhravajici_hrac == 2){
						printf("White wins.\n");
					}
					else{
						printf("Tie.\n");
					}
					
				}
			}
			//porovnani karet hracu, kteri nemaji nic...
			//porovnavam od nevyssi karty a pokud se lisi, zname viteze
			else if(hodnoceni1 == 1){
				int vyhravajici_hrac = 0;
				for(int i = 8; i >= 0; i -= 2){
					if(pole1[i] > pole2[i]){
						vyhravajici_hrac = 1;
						break;
					}
					else if(pole1[i] < pole2[i]){
						vyhravajici_hrac = 2;
						break;
					}
				}
				if(vyhravajici_hrac == 1){
					printf("Black wins.\n");
				}
				else if(vyhravajici_hrac == 2){
					printf("White wins.\n");
				}
				else{
					printf("Tie.\n");
				}
			}
			
			//pokud maji oba hraci barvu...
			else if(hodnoceni1 == 6){
				int vyhravajici_hrac = 0;
				for(int i = 8; i >= 0; i -= 2){
					if(pole1[i] > pole2[i]){
						vyhravajici_hrac = 1;
						break;
					}
					else if(pole1[i] < pole2[i]){
						vyhravajici_hrac = 2;
						break;
					}
				}
				if(vyhravajici_hrac == 1){
					printf("Black wins.\n");
				}
				else if(vyhravajici_hrac == 2){
					printf("White wins.\n");
				}
				else{
					printf("Tie.\n");
				}
			}
			
			//podminky vychazi ze zadani... Hodnoceni podle nejvyssiho cisla v trojici/cterici/postupce
			else{
				if(nejvyssi1 > nejvyssi2){
					printf("Black wins.\n");
				}
				else if(nejvyssi1 < nejvyssi2){
					printf("White wins.\n");
				}
				else{
					printf("Tie.\n");
				}
			}
		}
	}
	
	return 0;
}