#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//typ car: 11, 1, 10
void cary(int s, int typ){
	
		if(typ != 1){
			printf("|");
		}
		else{
			printf(" ");
		}

		for(int i = 0; i < s; i++){
			printf(" ");
		}

		if(typ != 10){
			printf("|");
		}
		else{
			printf(" ");
		}
}

//typ pomlcek: 1, 0
void pomlcky(int s, int typ){
	printf(" ");
	for(int i = 0; i < s; i++){
		if(typ == 1){
			printf("-");
		}
		else{
			printf(" ");
		}
	}
	printf(" ");
}

int main(){
	int znak = getchar();
	//priznak konce programu
	int konec = 0;
	int pocet_cisel = 0;
	int pole_cislo[9];

	//nekonecna smycka ukoncena breakem
	while(1){
		//velikost - prevod na cislo
		int s = znak - 48;
		if(s < 0 || s > 9){
			znak = getchar();
			continue;
		}
		//priznak konce
		if(s == 0){
			konec = 1;
		}
		else{
			konec = 0;
		}
		
		znak = getchar();
		//pokud je dalsi znak 0, pak je zadana velikost deset
		if(znak == 48){
			s = 10;
			znak = getchar();
		}
                //zde jeden znak ignoruji -> tohle je oddelovac mezi prvnim a druhym cislem, takze nactu cislo nove
		znak = getchar();

		//ignorace vsech naku ktere nejsou cislo
		while(znak < 48 || znak > 57){
			znak = getchar();
		}
		//jedine zde muze skoncit cyklus, pokud bylo zadano "0 0"
		if(konec && znak == 48){
			return 0;
		}

		while(znak != 10){
			//pokud se nejedna o prvni znak, tak je pred kazdym znakem sloupec mezer
			if(pocet_cisel != 0){
				printf(" ");
			}
			pole_cislo[pocet_cisel] = znak;
			
			//1, 4, 
			if(znak == 49 || znak == 52){
				pomlcky(s, 0);
			}

            //0, 2, 3, 5, 6, 7, 8, 9
			else{
				pomlcky(s,1);
			}

			pocet_cisel++;
			znak = getchar();
		}
 
		printf("\n");
		for(int j = 0; j < s; j++){
			for(int i = 0; i < pocet_cisel; i++){
				znak = pole_cislo[i];
				if(i != 0){
					printf(" ");
				}

				//1, 2, 3, 7
				if(znak == 49 || znak == 50 || znak == 51 || znak == 55){
					cary(s,1);
				}

				//5, 6
				else if(znak == 53 || znak == 54){
					cary(s, 10);
				}

				//0, 4, 8, 9
				else{
					cary(s, 11);
				}
			}
			printf("\n");
		}
		for(int i = 0; i < pocet_cisel; i++){
			znak = pole_cislo[i];
			if(i != 0){
				printf(" ");
			}
			//0, 1, 7
			if(znak == 48 || znak == 49 || znak == 55){
				pomlcky(s, 0);
			}

			//2, 3, 4, 5, 6, 8, 9
			else{
				pomlcky(s, 1);
			}
		}
		printf("\n");
		
		for(int j = 0; j < s; j++){
			for(int i = 0; i < pocet_cisel; i++){
			znak = pole_cislo[i];
				if(i != 0){
					printf(" ");
				}
				//0, 6, 8
				if(znak == 48 || znak == 54 || znak == 56){
					cary(s, 11);
				}

				//2
				else if(znak == 50){
					cary(s, 10);
				}

				//1, 3, 4, 5, 7, 9
				else{
					cary(s, 01);
				}
			}
			printf("\n");
		}


		for(int i = 0; i < pocet_cisel; i++){
			znak = pole_cislo[i];
			if(i != 0){
				printf(" ");
			}

			//1, 4, 7
			if(znak == 49 || znak == 52 || znak == 55){
				pomlcky(s, 0);
			}

			//0, 2, 3, 5, 6, 8, 9
			else{
				pomlcky(s, 1);
			}
		}

		printf("\n\n");
		pocet_cisel = 0;
		znak = getchar();
	}
}
