#include <stdio.h>
#include <stdlib.h>

int main(){
	int znak = getchar();
	int prazdna_deska = 1;
	int figurka = 0;
	int cislo_partie = 1;
	//pokud je bily v sachu, check == 1
	//pokud je cerny v sachu, check == 2
	//pokud zadny, tak check == 0;
	int check = 0;
	while(1){
		//je potreba pole 8x8 znaku
		int pole[8][8];
		//preskoceni vsech nepatricnych znaku na zacatku i na konci
		//nikdy by k tomu snad nemelo dojit
		while(znak != '.' && znak != 'r' && znak != 'n' && znak != 'b' && znak != 'q' && znak != 'k' && znak != 'p' && znak != 'R' && znak != 'N' && znak != 'B' && znak != 'Q' && znak != 'K' && znak != 'P'){
			znak = getchar();
			//konec programu
		}
		//nacteni sachove desky do pole
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				//preskoceni vsech nepatricnych znaku na zacatku i na konci
				pole[i][j] = znak;
				if(znak != '.'){
					//deska neni prazdna
					prazdna_deska = 0;
				}
				znak = getchar();
			}
			//preskoceni prazdnyho radku na konci partie
			znak = getchar();
		}
		//zde by mel vzdy program koncit
		if(prazdna_deska){
			return 0;
		}
		else{
			//nevime co je na dalsi desce, proto ji zatim povazujeme za prazdnou
			prazdna_deska = 1;
		}
		
		//postupne projdeme celou hraci desku a zjistime, jestli nekdo neohrozuje nejakyho krale
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				if(pole[i][j] != '.'){
					//v kazde podmince je nejdrive inicializace promenne radky i promenne sloupce
					int radky;
					int sloupce;
					figurka = pole[i][j];
					//zde prijde vyhodnoceni, asi by to melo byt ve funkci, ale...
					//pesak
					//.p.//
					//K.K//
					if(figurka == 'p'){
						if(((j > 0) && (i < 7)) && (pole[i+1][j-1] == 'K')){
							check = 1;
						}
						if(((j < 7) && (i < 7)) && (pole[i+1][j+1] == 'K')){
							check = 1;
						}
					}
					
					//pesak
					//k.k//
					//.P.//
					else if(figurka == 'P'){
						if(((j > 0) && (i > 0)) && (pole[i-1][j-1] == 'k')){
							check = 2;
						}
						if(((j < 7) && (i > 0)) && (pole[i-1][j+1] == 'k')){
							check = 2;
						}
					}
					
					//vezka
					//.K.//
					//KrK//
					//.K.//
					else if(figurka == 'r'){
						//-> = i zustava stejny
						if(j+1 < 8){
							for(int k = j+1; k < 8; k++){
								if(pole[i][k] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//<-
						if(j-1 >= 0){
							for(int k = j-1; k >= 0; k--){
								if(pole[i][k] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//v = j zustava stejny, meni se radek
						if(i+1 < 8){
							for(int k = i+1; k < 8; k++){
								if(pole[k][j] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
						//^
						if(i-1 >= 0){
							for(int k = i-1; k >= 0; k--){
								if(pole[k][j] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
					}
					
					//vezka
					//.k.//
					//kRk//
					//.k.//
					else if(figurka == 'R'){
						//-> = i zustava stejny
						if(j+1 < 8){
							for(int k = j+1; k < 8; k++){
								if(pole[i][k] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//<-
						if(j-1 >= 0){
							for(int k = j-1; k >= 0; k--){
								if(pole[i][k] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//v = j zustava stejny, meni se radek
						if(i+1 < 8){
							for(int k = i+1; k < 8; k++){
								if(pole[k][j] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
						//^
						if(i-1 >= 0){
							for(int k = i-1; k >= 0; k--){
								if(pole[k][j] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
					}
					
					//strelec
					//K.K//
					//.b.//
					//K.K//
					else if(figurka == 'b'){
						//^->
						radky = i-1;
						sloupce = j+1;
						while((radky >= 0) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce++;
						}
						
						radky = i-1;
						sloupce = j-1;
						//<-^
						while((radky > 0) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce--;
						}
						
						radky = i+1;
						sloupce = j+1;
						//v->
						while((radky < 8) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce++;
						}
						
						radky = i+1;
						sloupce = j-1;
						//<-v
						while((radky < 8) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce--;
						}
						
					}
					
					//strelec
					//k.k//
					//.B.//
					//k.k//
					else if(figurka == 'B'){
						//^->
						radky = i-1;
						sloupce = j+1;
						while((radky >= 0) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce++;
						}
						
						radky = i-1;
						sloupce = j-1;
						//<-^
						while((radky >= 0) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce--;
						}
						
						radky = i+1;
						sloupce = j+1;
						//v->
						while((radky < 8) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce++;
						}
						
						radky = i+1;
						sloupce = j-1;
						//<-v
						while((radky < 8) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce--;
						}	
					}
					
					//kralovna
					//KKK//
					//KqK//
					//KKK//
					else if(figurka == 'q'){
						//-> = i zustava stejny
						if(j+1 < 8){
							for(int k = j+1; k < 8; k++){
								if(pole[i][k] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//<-
						if(j-1 >= 0){
							for(int k = j-1; k >= 0; k--){
								if(pole[i][k] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//v = j zustava stejny, meni se radek
						if(i+1 < 8){
							for(int k = i+1; k < 8; k++){
								if(pole[k][j] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
						//^
						if(i-1 >= 0){
							for(int k = i-1; k >= 0; k--){
								if(pole[k][j] == 'K'){
									check = 1;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
						//^->
						radky = i-1;
						sloupce = j+1;
						while((radky > 0) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce++;
						}
						
						radky = i-1;
						sloupce = j-1;
						//<-^
						while((radky > 0) && (sloupce > 0)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce--;
						}
						
						radky = i+1;
						sloupce = j+1;
						//v->
						while((radky < 8) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce++;
						}
						
						radky = i+1;
						sloupce = j-1;
						//<-v
						while((radky < 8) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'K'){
								check = 1;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce--;
						}
					} //konec q
					
					//kkk//
					//kQk//
					//kkk//
					else if(figurka == 'Q'){
						//-> = i zustava stejny
						if(j+1 < 8){
							for(int k = j+1; k < 8; k++){
								if(pole[i][k] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//<-
						if(j-1 >= 0){
							for(int k = j-1; k >= 0; k--){
								if(pole[i][k] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[i][k] != '.'){
									break;
								}
							}
						}
						
						//v = j zustava stejny, meni se radek
						if(i+1 < 8){
							for(int k = i+1; k < 8; k++){
								if(pole[k][j] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
						//^
						if(i-1 >= 0){
							for(int k = i-1; k >= 0; k--){
								if(pole[k][j] == 'k'){
									check = 2;
								}
								//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
								else if(pole[k][j] != '.'){
									break;
								}
							}
						}
						
						//^->
						radky = i-1;
						sloupce = j+1;
						while((radky >= 0) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce++;
						}
						
						radky = i-1;
						sloupce = j-1;
						//<-^
						while((radky >= 0) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky--;
							sloupce--;
						}
						
						radky = i+1;
						sloupce = j+1;
						//v->
						while((radky < 8) && (sloupce < 8)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce++;
						}
						
						radky = i+1;
						sloupce = j-1;
						//<-v
						while((radky < 8) && (sloupce >= 0)){
							if(pole[radky][sloupce] == 'k'){
								check = 2;
							}
							//pokud stoji vezce nekdo v ceste, tak vezka dal nedosahne a cyklus konci
							else if(pole[radky][sloupce] != '.'){
								break;
							}
							radky++;
							sloupce--;
						}
					} //konec Q

					//kun - muze preskakovat, veskere varianty 'L'
					else if(figurka == 'n'){
						//|
						//|_ (radek+2, sloupec+1)
						radky = i+2;
						sloupce = j+1;
						if((radky < 8 && sloupce < 8) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						//|_ _ (radek+1, sloupec+2)
						radky = i+1;
						sloupce = j+2;
						if((radky < 8 && sloupce < 8) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						// |
						//_| (radek+2, sloupec-1)
						radky = i+2;
						sloupce = j-1;
						if((radky < 8 && sloupce >= 0) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						//_ _| (radek+1, sloupec-2)
						radky = i+1;
						sloupce = j-2;
						if((radky < 8 && sloupce >= 0) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						//|´ (radek-2, sloupec+1)
						//|
						radky = i-2;
						sloupce = j+1;
						if((radky >= 0 && sloupce < 8) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						//|´´ (radek-1, sloupec+2)
						radky = i-1;
						sloupce = j+2;
						if((radky >= 0 && sloupce < 8) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						//´| (radek-2, sloupec-1)
						// |
						radky = i-2;
						sloupce = j-1;
						if((radky >= 0 && sloupce >= 0) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
						
						//´´| (radek-1, sloupec-2)
						radky = i-1;
						sloupce = j-2;
						if((radky >= 0 && sloupce >= 0) && (pole[radky][sloupce] == 'K')){
							check = 1;
						}
					}

					//kun - muze preskakovat, veskere varianty 'L'
					else if(figurka == 'N'){
						//|
						//|_ (radek+2, sloupec+1)
						radky = i+2;
						sloupce = j+1;
						if((radky < 8 && sloupce < 8) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						//|_ _ (radek+1, sloupec+2)
						radky = i+1;
						sloupce = j+2;
						if((radky < 8 && sloupce < 8) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						// |
						//_| (radek+2, sloupec-1)
						radky = i+2;
						sloupce = j-1;
						if((radky < 8 && sloupce >= 0) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						//_ _| (radek+1, sloupec-2)
						radky = i+1;
						sloupce = j-2;
						if((radky < 8 && sloupce >= 0) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						//|´ (radek-2, sloupec+1)
						//|
						radky = i-2;
						sloupce = j+1;
						if((radky >= 0 && sloupce < 8) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						//|´´ (radek-1, sloupec+2)
						radky = i-1;
						sloupce = j+2;
						if((radky >= 0 && sloupce < 8) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						//´| (radek-2, sloupec-1)
						// |
						radky = i-2;
						sloupce = j-1;
						if((radky >= 0 && sloupce >= 0) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
						
						//´´| (radek-1, sloupec-2)
						radky = i-1;
						sloupce = j-2;
						if((radky >= 0 && sloupce >= 0) && (pole[radky][sloupce] == 'k')){
							check = 2;
						}
					} //konec kun
				} // konec if '.'
			}
		} //konec cyklu pres vsechny ctverce na desce
		
		//vypis - kdo ze je vlastne na dane desce v sachu...
		if(check == 1){
			printf("Game #%i: white king is in check.\n", cislo_partie);
		}
		else if(check == 2){
			printf("Game #%i: black king is in check.\n", cislo_partie);
		}
		else{
			printf("Game #%i: no king is in check.\n", cislo_partie);
		}
		
		//dalsi partie...
		check = 0;
		cislo_partie++;
	} // konec nekonecnyho cyklu
	//sem se nikdy program nedostane, snad
	return 0;
}