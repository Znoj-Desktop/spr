#include <stdio.h>
#include <stdlib.h>

//max = 75*3
#define max 225

//globalni promenne vyuzivane v alg
int w, h;
bool pole[max][max];
bool obarveno[max][max];

//lehce modifikovane seminkove vyplnovani
//Otestuj zda bod ji� nebyl vypln�n nebo nele�� na hranici.
//Pokud ano skon��, pokud ne vypl� tento bod a ur�i v�echny sousedn� body.
//Pro v�echny sousedn� body znovu pou�ij tento algoritmus.
int seminkove_vyplnovani(int x, int y){
	//pokud jsem mimo hranice, pak vracim -1,
	//ktera znaci, ze se nejedna o kruznici
	if(x >= 3*w || x < 0 || y < 0 || y >= 3*h){
		return -1;
	} 
	//pokud narazim na jiz obarveny uzel nebo okraj kruznice, tak vracim 0
	else if(obarveno[y][x] || pole[y][x]){
		return 0;
	} 
	else{
		//pocatecni uzel nastavim jako obarveny
		obarveno[y][x] = true;
		//jdu se podivat do vsech smeru
		int vpravo = seminkove_vyplnovani(x + 1, y);
		int vlevo = seminkove_vyplnovani(x - 1, y);
		int nahore = seminkove_vyplnovani(x, y + 1);
		int dole = seminkove_vyplnovani(x, y - 1);
		//pokud nejsem mimo pole, pricitam
		if(vpravo >= 0 && vlevo >= 0 && nahore >= 0 && dole >= 0){
			//prictu 1 za kazdy novy ctverecek
			return (vpravo + vlevo + nahore + dole + 1);
		}
		else{
			//znacim ze se nejedna o kruznici
			return -1;
		}
	}
} //konec seminkoveho vyplnovani


int main(){
	int poradi = 1;
	int nejdelsi;
	int kruznic;
	char znak; 

	//nekonecny cyklus pro nacitani vstupu
	while(scanf("%i %i", &w, &h) > 0){
		//zde je konec programu--------------------------
		if(w == 0 && h == 0){
			return 0;
		}
		
		//nulovani
		nejdelsi = 0;
		kruznic = 0;

		//nulovani poli - jen pro jistotu...
		for(int i = 0; i < max; i++){
			for(int j = 0; j < max; j++){
				pole[i][j] = 0;
				obarveno[i][j] = 0;
			}
		}

		//ulozeni vstupu do pole
		for(int i = 0; i < h; i++){
			for(int j = 0; j < w; j++){
				znak = getchar();
				//preskoceni nepatricnych znaku
				while(znak !='/' && znak != '\\'){
					znak = getchar();
				}
				//zakresleni znaku '/' do ctverce 3x3
				if(znak == '/'){
					pole[3*i + 0][3*j + 2] = true;
					pole[3*i + 2][3*j + 0] = true;
				}
				//zakresleni znaku '\' do ctverce 3x3
				else{
					pole[3*i + 0][3*j + 0] = true;
					pole[3*i + 2][3*j + 2] = true;
				}
				//pro '/' i '\' je prostredni ctverec stejny
				pole[3*i + 1][3*j + 1] = true;
			}
		}

		//pro kazdy znak zavolej funkci seminkove_vyplnovani
		for(int i = 0; i < 3*h; i++){
			for(int j = 0; j < 3*w; j++){
				int velikost_kruznice = seminkove_vyplnovani(j, i);
				//je potreba velikost vydelit 3
				velikost_kruznice /= 3;
				if(velikost_kruznice > 0){
					if(velikost_kruznice > nejdelsi){
						nejdelsi = velikost_kruznice;
					}
					kruznic++;
				}
			}
		}

		//finalni vypis
		printf("Maze #%d:\n", poradi++);

		if(kruznic == 0){
			printf("There are no cycles.\n\n");
		}
		else{
			printf("%d Cycles; the longest has length %d.\n\n", kruznic, nejdelsi);
		}
	}

	return 0;
}