#include <stdio.h>
#include <stdlib.h>

int main(){
	
	//2 .. 21 cisel
	int cisla[21];
	//max 20 znaminek
	char znaminka[20];
	//promenna ma max 8 znaku
	char promenna[8];

	//nacteni prvniho cisla
	while(scanf("%i", &cisla[0]) > 0){

	//nacteni vstupu
	int poradi_cisla = 1;
	int poradi_znaku = 0;
	int znak;
	do{
		//---printf("%i\n", cisla[poradi_cisla-1]);
		//preskoceni nepatricnych znaku - predevsim mezer
		do{
			znak = getchar();
			if(znak == '='){
				break;
			}
		}while(znak != '+' && znak != '-' && znak != '*' && znak != '/');
		if(znak == '='){
			break;	
		}
		znaminka[poradi_znaku++] = (char)znak;
		//---printf("%c\n", znaminka[poradi_znaku-1]);
	}while(scanf("%i", &cisla[poradi_cisla++]) > 0);

	//ulozeni promenne pro vypsani do vysledku
	int c = 0;
	//to co je za "rovna se" je promenna
	while(znak != 10 && znak != EOF){
		if(znak >= 'a' && znak <= 'z'){
			promenna[c++] = (char)znak;
		}
		znak = getchar();
	}
	promenna[c++] = '\0';

	//---printf("%s\n", promenna);

	//vypsani zadani:
	printf("%i", cisla[0]);
	for(int k = 1; k < poradi_cisla; k++){
		printf(" %c %i", znaminka[k-1], cisla[k]);
	}
	printf(" = %s\n", promenna);
	for(int i = 0; i < poradi_znaku; i++){
			if(znaminka[i] == '*'){
				cisla[i] *= cisla[i+1]; 
				//preskladani cisel a znaku
				for(int j = i+1; j < (poradi_cisla); j++){
					cisla[j] = cisla[j+1];
					znaminka[j-1] = znaminka[j];
				}
				//zmenseni pole o jednicku
				poradi_cisla--;
				poradi_znaku--;
				printf("%i", cisla[0]);
				for(int k = 1; k < poradi_cisla; k++){
					printf(" %c %i", znaminka[k-1], cisla[k]);
				}
				//protozejsme posunuli znaminka o 1 zpet, tak se i zpet vratime
				i--;
				printf(" = %s\n", promenna);
			}
			else if(znaminka[i] == '/'){
				cisla[i] /= cisla[i+1]; 
				//preskladani cisel a znaku
				for(int j = i+1; j < (poradi_cisla); j++){
					cisla[j] = cisla[j+1];
					znaminka[j-1] = znaminka[j];
				}
				//zmenseni pole o jednicku
				poradi_cisla--;
				poradi_znaku--;
				printf("%i", cisla[0]);
				for(int k = 1; k < poradi_cisla; k++){
					printf(" %c %i", znaminka[k-1], cisla[k]);
				}
				//protozejsme posunuli znaminka o 1 zpet, tak se i zpet vratime
				i--;
				printf(" = %s\n", promenna);
			}
		}


	for(int i = 0; i < poradi_znaku; i++){
			if(znaminka[i] == '+'){
				cisla[i] += cisla[i+1]; 
				//preskladani cisel a znaku
				for(int j = i+1; j < (poradi_cisla); j++){
					cisla[j] = cisla[j+1];
					znaminka[j-1] = znaminka[j];
				}
				//zmenseni pole o jednicku
				poradi_cisla--;
				poradi_znaku--;
				printf("%i", cisla[0]);
				for(int k = 1; k < poradi_cisla; k++){
					printf(" %c %i", znaminka[k-1], cisla[k]);
				}
				//protozejsme posunuli znaminka o 1 zpet, tak se i zpet vratime
				i--;
				printf(" = %s\n", promenna);
			}
			else if(znaminka[i] == '-'){
				cisla[i] -= cisla[i+1]; 
				//preskladani cisel a znaku
				for(int j = i+1; j < (poradi_cisla); j++){
					cisla[j] = cisla[j+1];
					znaminka[j-1] = znaminka[j];
				}
				//zmenseni pole o jednicku
				poradi_cisla--;
				poradi_znaku--;
				printf("%i", cisla[0]);
				for(int k = 1; k < poradi_cisla; k++){
					printf(" %c %i", znaminka[k-1], cisla[k]);
				}
				//protozejsme posunuli znaminka o 1 zpet, tak se i zpet vratime
				i--;
				printf(" = %s\n", promenna);
			}
		}
		printf("\n");
	}
	return 0;
}