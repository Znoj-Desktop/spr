#include <stdlib.h>
#include <stdio.h>
 
//max delka v cm (100m == 10 000cm)
#define MAX_DELKA 10001
//10000(nejdelsi_mozna_rada)/100(delka_nejkratsiho_auta) = 100 * 2(2 rady)
//201 haze runtime error - 220 je uz ok
#define MAX_AUT 220

//pole priznaku - zde se uchovavaji informace o velikosti leve rady
bool pole[MAX_AUT][MAX_DELKA];
//uchovavam si velikosti aut tak jak jdou za sebou
int pole_delek[MAX_AUT];
//pro kazde auto si pamatuju velikost leve rady pri prijezdu
int predchozi_auto[MAX_AUT][MAX_DELKA];
//delka rady v cm ( 100 - 10 000)
int delka_rady = 0;
//poradi posledniho auta
int posledni_auto = 0;
int delka_posledniho_auta = 0;
int velikost_celkem = 0; 


//projiti n-teho radku, kde n je poradi auta
void f_vypocet(int delka_auta, int pocitadlo){
	velikost_celkem += delka_auta;
	pole_delek[pocitadlo] = delka_auta;
	//projdu cely radek
	for (int i = 0; i <= delka_rady; i++) {
		//pokud v predchozim radku
		//pokud se vleze auto vlevo (za predchozi nebo za hodnotu pole[0][0] ktera je true,
		//pak tam zajede a zaznaci to do pole
		if (pole[pocitadlo - 1][i - delka_auta] && (i >= delka_auta)) {
			//zaznacim misto kde auto konci do pole priznaku
			pole[pocitadlo][i] = 1;
			//zaznacim si kde konci predchozi auto v leve rade
			predchozi_auto[pocitadlo][i] = i - delka_auta;
			//vzdy si pamatuju ktery auto jsem zpracovaval jako posledni
			posledni_auto = pocitadlo;
			delka_posledniho_auta = i;
			//printf("L: %2d, %4d | %4d, %4d -> %d\n", pocitadlo, delka_auta, i, velikost_celkem-i, i-delka_auta);
		} 
		//pokud se vleze auto vpravo, tak tam zajede a zaznaci to
		else if (pole[pocitadlo - 1][i] && (velikost_celkem - i <= delka_rady)) {
			pole[pocitadlo][i] = 1;
			//zaznacim si kde konci predchozi auto v leve rade
			predchozi_auto[pocitadlo][i] = i;
			//vzdy si pamatuju posledni auto
			posledni_auto = pocitadlo;
			delka_posledniho_auta = i;
			//printf("R: %2d, %4d | %4d, %4d -> %d\n", pocitadlo, delka_auta, i, velikost_celkem-i, i);
		}
	}
}


int f_vypis(int leva, int prava){
	//"zasobnik"
	bool vypis[MAX_AUT];

	for(int m = posledni_auto; m > 0; m--){
		//printf("leva: %d, prava: %d\n", leva, prava);
		//pokud se konec leveho auta lisi od konce leveho auta pred timto, pak jedu jiste vlevo
		if(predchozi_auto[m][leva] != leva){
			//printf("x %d, %d, %d, %d\n",m, leva, predchozi_auto[m][leva], pole_delek[m]);
			leva -= pole_delek[m];
			vypis[m] = 0;
		}
		//pokud se konec auta vlevo nelisi od konce auta vlevo pred tim nez tam prijelo toto auto
		//pak toto auto jelo jiste vpravo
		else{
			//printf("y %d, %d, %d, %d\n",m, prava, predchozi_auto[m][prava], pole_delek[m]);
			prava -= pole_delek[m];
			vypis[m] = 1;
		}
	}
	//vyprazdneni zasobniku + vypis
	for(int m = 1; m <= posledni_auto; m++){
		if(vypis[m] == 0){
			printf("port\n");
		}
		else{
			printf("starboard\n");
		}
	}
}


int main() {
	//delka auta (100 - 10 000 cm)
	int delka_auta = 0;
	int pocet_vstupu = 0;
	int pocitadlo = 1;

	scanf("%d", &pocet_vstupu);

	for(int i = 0; i < pocet_vstupu; i++){

		scanf("%d", &delka_rady);
		delka_rady *= 100;

		//vynulovani hodnot
		for(int k = 0; k < MAX_AUT; k++){
			for(int l = 0; l < MAX_DELKA; l++){
				pole[k][l] = 0;
			}
			pole_delek[k] = 0;
		}
		//prvni hodnota je vzdy true, takze pokud je prvni auto dostatecne maly,
		//tak muze vjet na lod nebo co to je
		pole[0][0] = 1;

		pocitadlo = 1;
		delka_auta = 0;
		velikost_celkem = 0; 
		posledni_auto = 0;
		delka_posledniho_auta = 0;

		scanf("%d", &delka_auta);
		//postupne nacitani hodnot a kontrola konce
		while (delka_auta != 0) {
			//zaznamenani vsech priznaku pro aktualne zpracovavane auto
			f_vypocet(delka_auta, pocitadlo);
			pocitadlo++;
			scanf("%d", &delka_auta);
		}

		//ulozeni velikosti obou front na lodi
		int leva = delka_posledniho_auta;
		int prava = velikost_celkem-delka_posledniho_auta;
		//vypis
		printf("%d\n", posledni_auto);
		f_vypis(leva, prava);
		
		//mezi jednotlivymi vystupy je mezera
		if (i != pocet_vstupu-1) {
		  printf("\n");
		}
	}
	return 0;
}